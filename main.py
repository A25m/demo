from flask import Flask, jsonify, request
app = Flask(__name__)
users = []
posts = []
@app.route('/users', methods=['POST'])
def create_user():
    user = request.get_json()
    users.append(user)
    return jsonify(user), 201
    @app.route('/users', methods=['GET'])
    def get_users():
        return jsonify(users)
        @app.route('/users/', methods=['GET'])
        def get_user(user_id):
            for user in users:
                if user['id'] == user_id:
                    return jsonify(user)
                    return jsonify({'error': 'User not found'}), 404
                @app.route('/users/', methods=['PUT'])
                def update_user(user_id):
                    user = request.get_json()
                    for i in range(len(users)):
                        if users[i]['id'] == user_id:
                            users[i] = user
                            return jsonify(user)
                            return jsonify({'error': 'User not found'}), 404
                            @app.route('/users/', methods=['DELETE'])
                            def delete_user(user_id):
                                for i in range(len(users)):
                                    if users[i]['id'] == user_id:
                                        del users[i]
                                        return '', 204
                                        return jsonify({'error': 'User not found'}), 404
                                        @app.route('/posts', methods=['POST'])
                                        def create_post():
                                            post = request.get_json()
                                            posts.append(post)
                                            return jsonify(post), 201
                                            @app.route('/posts', methods=['GET'])
                                            def get_posts():
                                                return jsonify(posts)
                                                @app.route('/posts/', methods=['GET'])
                                                def get_post(post_id):
                                                    for post in posts:
                                                        if post['id'] == post_id:
                                                         return jsonify(post)
                                                         return jsonify({'error': 'Post not found'}), 404
                                                         @app.route('/posts/', methods=['PUT'])
                                                         def update_post(post_id):
                                                             post = request.get_json()
                                                             for i in range(len(posts)):
                                                                     if posts[i]['id'] == post_id:
                                                                         posts[i] = post
                                                                         return jsonify(post)
                                                                         return jsonify({'error':'Post not found'}), 404
                                                                         @app.route('/posts/', methods=['DELETE'])
                                                                         def delete_post(post_id):
                                                                             for i in range(len(posts)):
                                                                                 if posts[i]['id'] == post_id:
                                                                                     del posts[i]
                                                                                     return '', 204
                                                                                     return jsonify({'error': 'Post not found'}), 404
                                                                                     if __name__ == '__main__':
                                                                                         app.run(debug=True)
